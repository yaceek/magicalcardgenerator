﻿using CardGenerator2.Model.Config;
using CardGenerator2.Output;
using CardGenerator2.Renderer;
using CardGenerator2.Services;
using CardGenerator2.Utils;
using NLog;

namespace CardGenerator2;

// https://hephaestus.dev/proximity/resources/set_symbols/1.0.0/set_symbol%2Fgrn.svg

internal static class Program {
    private static readonly Logger Log = LogManager.GetCurrentClassLogger();

    private static void Main() {
        try {
            LogManager.Setup().LoadConfiguration(builder => {
                builder.ForLogger().FilterMinLevel(LogLevel.Info).WriteToConsole(NLog.Layouts.Layout.FromString("${longdate}: ${message}"));
                builder.ForLogger().FilterMinLevel(LogLevel.Debug).WriteToFile(fileName: $"logs/{DateTime.Now:yyyyMMddHHmmss}.log");
            });

            Log.Info("Loading input...");
            var inputs = JsonUtil.Load<Input[]>("input.json");

            Log.Info("Processing cards...");
            var output = (Output.Output) (ConfigurationService.Instance.Config.ConfigParameters.OutputFormat == Format.Pdf ? new PdfOutput() : new ImageOutput());
            foreach (var input in inputs) {
                Log.Info($"  Loading card data from scryfall '{input.Set}' / '{input.Number ?? input.Name}'");
                var card = input.Number != null ? ScryfallApi.FindCardByNumber(input.Set, input.Number) : ScryfallApi.FindCardByName(input.Set, input.Name!);
                if (card != null) {
                    var cardfaces = CardPreprocessor.PreprocessCard(card.AsObject(), input.Overrides);
                    foreach (var cardface in cardfaces) {
                        try {
                            new CardRenderer3(output).RenderCardFace(cardface, input.Count);
                        }
                        catch (Exception ex) {
                            Log.Error($"Error during card processing [{ex.Message}]");
                            Log.Debug(ex);
                        }
                    }
                }
                else {
                    Log.Warn($"  Card {input.Set}/{input.Number ?? input.Name} not found in scryfall data!");
                }
            }

            Log.Info("Finalizing output...");
            output.Done();
            Log.Info("Done!");
        }
        catch (Exception ex) {
            Log.Error(ex, $"Error during processing [{ex.Message}]");
        }
    }
}
