﻿using SixLabors.ImageSharp;

namespace CardGenerator2.Model.Text;

public class Paragraph {
    public List<Word> Words { get; } = new();

    // rendering properties
    public RenderParameters? Parameters { get; set; }
    public Image? Image { get; set; }
    public Rectangle Bounds { get; set; }
    public bool Background { get; set; }
}
