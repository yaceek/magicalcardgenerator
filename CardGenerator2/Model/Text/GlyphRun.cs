﻿using SixLabors.ImageSharp;

namespace CardGenerator2.Model.Text;

public enum GlyphType {
    Regular, Italic, Bold, Symbol
}

public class GlyphRun {
    public GlyphType Type { get; }
    public string Text { get; }
    public Color Color { get; set; } = Color.Black;

    // rendering properties
   public int Width { get; set; }
    public int AdjustLeft { get; set; }

    public GlyphRun(string text, GlyphType type) {
        Text = text;
        Type = type;
    }
}
