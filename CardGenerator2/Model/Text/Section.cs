﻿using System.Diagnostics.CodeAnalysis;
using CardGenerator2.Model.Config;
using SixLabors.Fonts;
using SixLabors.ImageSharp;

namespace CardGenerator2.Model.Text;


public class RenderParameters {
    public int Width { get; init; }
    public int Height { get; init; }
    public Margins ParagraphMargins { get; init; } = new();
    public float LineGapRatio { get; init; }
    public float ParagraphGapRatio { get; init; }
    public int Size { get; init; }
    public Dictionary<GlyphType, (Font font, string name, int ascender, int descender, int linegap, int height, int lineheight)> Fonts { get; init; } = new();
}

[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class Section {
    public int Idx { get; }
    public string Key { get; }
    public string? Label { get; init; }
    public string? Extra { get; init; }
    public bool Indent { get; init; }
    public List<Paragraph> Paragraphs { get; }

    // rendering properties
    public RenderParameters? Parameters { get; set; }
    public Image? ContentsImage { get; set; }
    public Image? IndicatorImage { get; set; }
    public Rectangle Bounds { get; set; }
    public int Middle { get; set; }

    public Section(int idx, string key, List<Paragraph> paragraphs) {
        Paragraphs = paragraphs;
        Idx = idx;
        Key = key;
    }
}
