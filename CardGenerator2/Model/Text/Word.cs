﻿using SixLabors.ImageSharp;

namespace CardGenerator2.Model.Text;

public class Word {
    public List<GlyphRun> GlyphRuns { get; } = new();

    // rendering properties
    public Rectangle Bounds { get; set; }
}
