﻿using System.Diagnostics.CodeAnalysis;
using System.Text.Json.Nodes;
using CardGenerator2.Model.Config;
using CardGenerator2.Model.Text;
using CardGenerator2.Services;
using CardGenerator2.Utils;

namespace CardGenerator2.Model.Renderer;

[SuppressMessage("ReSharper", "UnusedMember.Global")]
[SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class CardFace {
    private readonly JsonObject _card;
    private readonly Dictionary<string, JsonObject> _faces = new();

    public Layout Layout { get; }
    public string Face { get; }
    public CardFace? OtherSide { get; set; }
    public string FullCardName => JsonUtil.String(_card["name"]);
    public List<Section> Text { get; }
    public Dictionary<string, string> Vars { get; } = new();

    public CardFace(JsonObject card, string face, Layout layout) {
        Face = face;
        Layout = layout;

        _card = card;
        if (_card["card_faces"] != null) {
            _faces["front"] = _card["card_faces"]![0]!.AsObject();
            _faces["back"] = _card["card_faces"]![1]!.AsObject();
        }
        else {
            _faces["front"] = _card;
        }

        // initialize text sections
        if (FieldContains("type_line", "Saga")) {
            Text = TextProcessor.SplitTextSaga(JsonUtil.String(_faces[Face]["oracle_text"]));
        }
        else if (FieldContains("type_line", "Class")) {
            Text = TextProcessor.SplitTextClass(JsonUtil.String(_faces[Face]["oracle_text"]));
        }
        else if (FieldContains("type_line", "Planeswalker")) {
            Text = TextProcessor.SplitTextPlaneswalker(JsonUtil.String(_faces[Face]["oracle_text"]));
        }
        else if (JsonUtil.Equals(_card["layout"], "leveler")) {
            Text = TextProcessor.SplitTextLeveler(JsonUtil.String(_faces[Face]["oracle_text"]));
        }
        else if (FieldContains("keywords", "Mutate")) {
            Text = TextProcessor.SplitTextMutate(JsonUtil.String(_faces[Face]["oracle_text"]), FieldTrue("remove_reminder"));
        }
        else if (FieldContains("keywords", "Prototype")) {
            Text = TextProcessor.SplitTextPrototype(JsonUtil.String(_faces[Face]["oracle_text"]), JsonUtil.String(_faces[Face]["flavor_text"]), FieldTrue("remove_reminder"));
        }
        else {
            Text = TextProcessor.SplitTextStandard(JsonUtil.String(_faces[Face]["oracle_text"]), JsonUtil.String(_faces[Face]["flavor_text"]), FieldTrue("remove_reminder"));
        }
    }

    public string? Field(string field) {
        return FieldNode(field)?.ToString();
    }

    public string Field(string field, int length) {
        var str = Field(field);
        return new string('0', length - str?.Length ?? 0) + str;
    }

    public bool HasField(string field) {
        return Field(field)?.Length > 0;
    }

    public bool FieldContains(string field, params string[] values) {
        return JsonUtil.Contains(FieldNode(field), values);
    }

    public bool FieldTrue(string field) {
        return Field(field)?.Equals("true") ?? false;
    }

    public int? FieldInt(string field) {
        var str = Field(field);
        return str != null ? int.Parse(str) : null;
    }

    private JsonNode? FieldNode(string field) {
        var parts = field.Split('|', StringSplitOptions.TrimEntries);
        return JsonUtil.Traverse(_faces[Face], parts) ?? JsonUtil.Traverse(_card, parts);
    }

    public string FieldColors(string field) {
        var f = _faces[Face][field] ?? _card[field];
        if (f == null) return "";
        var s = f is JsonArray ? string.Join("", JsonUtil.Arr(f).Select(v => v!.ToString()).OrderBy(c => c)) : JsonUtil.String(f);
        return SortColors(string.Join("", s.Where(c => c is 'W' or 'U' or 'B' or 'R' or 'G').Distinct()));
    }

    private static string SortColors(string colors) {
        return colors switch {
            "BU" => "UB",
            "BW" => "WB",
            "GR" => "RG",
            "RU" => "UR",
            "UW" => "WU",
            "BGR" => "BRG",
            "BGU" => "BGU",
            "BGW" => "WBG",
            "BRU" => "UBR",
            "BRW" => "RWB",
            "BUW" => "WUB",
            "GRU" => "GUR",
            "GRW" => "RGW",
            "GUW" => "GWU",
            "RUW" => "URW",
            "BGRU" => "UBRG",
            "BGRW" => "BRGW",
            "BGUW" => "GWUB",
            "BRUW" => "WUBR",
            "GRUW" => "RGWU",
            "BGRUW" => "WUBRG",
            _ => colors
        };
    }
}