﻿using System.Diagnostics.CodeAnalysis;
using System.Text.Json.Serialization;

namespace CardGenerator2.Model.Config;

#pragma warning disable CS8618

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
[SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Global")]
internal class Input {
    [JsonPropertyName("set")] public string Set { get; set; }
    [JsonPropertyName("count")] public int? Count { get; set; }
    [JsonPropertyName("name")] public string? Name { get; set; }
    [JsonPropertyName("num")] public string? Number { get; set; }
    [JsonPropertyName("overrides")] public Dictionary<string, string?> Overrides { get; set; } = new();
}

#pragma warning restore CS8618
