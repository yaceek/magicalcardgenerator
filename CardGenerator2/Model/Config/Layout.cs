﻿using System.Diagnostics.CodeAnalysis;
using SixLabors.ImageSharp;

namespace CardGenerator2.Model.Config;

#pragma warning disable CS8618

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
[SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Global")]
public class Layout {
    public Meta Meta { get; set; }
    public string Parent { get; set; }
    public Rectangle Dimensions { get; set; }
    public Dictionary<string, string> Vars { get; set; } = new();
    public FrameLayer[] FrameLayers { get; set; }
    public Span[] Spans { get; set; }
    public TextBox[] TextBoxes { get; set; }
    public string Dir { get; set; }
}

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
[SuppressMessage("ReSharper", "CollectionNeverUpdated.Global")]
[SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Global")]
public class Parent {
    public Dictionary<string, string> Vars { get; set; } = new();
    public Span[] Spans { get; set; }
}

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
[SuppressMessage("ReSharper", "InconsistentNaming")]
public class Meta {
    public int DPI { get; set; }
    public int Margin { get; set; }
}

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class FrameLayer {
    public string Id { get; set; }
    public string Type { get; set; }
    public string? Condition { get; set; }
    public Layer[]? Selector { get; set; }
}

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class Layer {
    public string Source { get; set; }
    public string? Condition { get; set; }
    public bool Fit { get; set; }
    public Position? Position { get; set; }
    public string? Mask { get; set; }
}

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class Position {
    public Rectangle Box { get; set; }
    public int Rotation { get; set; }
}

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class Span {
    public string Id { get; set; }
    public string? Condition { get; set; }
    public Position Position { get; set; }
    public Element[] Elements { get; set; }
}

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class Element {
    public TextElement? Text { get; set; }
    public ImgElement? Img { get; set; }
    public string? Condition { get; set; }
    public Align Align { get; set; }
}

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class TextElement {
    public string Value { get; set; }
    public Margins? Margins { get; set; }
    public float Adjust { get; set; }
    public FontParameters Font { get; set; }
    public Point? Shadow { get; set; }
}

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class ImgElement {
    public string Path { get; set; }
    public Margins? Margins { get; set; }
    public int Height { get; set; }
    public int Adjust { get; set; }
}

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class TextBox {
    public string Id { get; set; }
    public string Source { get; set; }
    public string? Condition { get; set; }
    public Position Position { get; set; }
    public FontParameters FontParameters { get; set; }
    public SectionParameters SectionParameters { get; set; }
    public Separator[]? Separators { get; set; }
    public string? BackgroundFiller { get; set; }
    public string? BackgroundMask { get; set; }
    public Indicator? Indicator { get; set; }
    public Fixed[]? FixedSections { get; set; }
}

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class FontParameters {
    public string? RegularFont { get; set; }
    public string? ItalicFont { get; set; }
    public string? BoldFont { get; set; }
    public string? Color { get; set; }
    public int Size { get; set; }
}

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class SectionParameters {
    public int Indentation { get; set; }
    public float LineGapRatio { get; set; }
    public float ParagraphGapRatio { get; set; }
    public Margins ParagraphMargins { get; set; }
}

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class Margins {
    public int Left { get; init; }
    public int Right { get; init; }
    public int Top { get; init; }
    public int Bottom { get; init; }
}

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
[SuppressMessage("ReSharper", "CollectionNeverUpdated.Global")]
public class Indicator {
    public int ImagePosition { get; set; } // TODO replace with margin relative to box
    public Dictionary<string, IndicatorImage> Images { get; set; }
    public int ImageSpacing { get; set; }
    public Label? Label { get; set; }
}

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class IndicatorImage {
    public string Path { get; set; }
    public int Adjust { get; set; }
}

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
[SuppressMessage("ReSharper", "CollectionNeverUpdated.Global")]
public class Label {
    public FontParameters Font { get; set; }
    public Dictionary<string, Point> Positions { get; set; }
}

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
[SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Global")]
public class Separator {
    public string? Image { get; set; }
    public int Height { get; set; }
    public Margins? Margins { get; set; }
    public Element[]? Elements { get; set; }
    public AlignVertical Align { get; set; } = AlignVertical.Center;
}

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class Fixed {
    public int? Idx { get; set; }
    public string? Key { get; set; }
    public int? Height { get; set; }
    public int? FontSize { get; set; }
}

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class Align {
    public AlignHorizontal H { get; set; }
    public AlignVertical V { get; set; }
}

public enum AlignHorizontal { Left, Right, Center }
public enum AlignVertical { Top, Bottom, Center, Middle }

#pragma warning restore CS8618
