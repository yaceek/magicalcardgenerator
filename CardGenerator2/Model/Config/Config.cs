﻿using System.Diagnostics.CodeAnalysis;

namespace CardGenerator2.Model.Config;

#pragma warning disable CS8618

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "CollectionNeverUpdated.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class Configuration {
    public ConfigParameters ConfigParameters { get; set; }
    public ScryfallProperties ScryfallProperties { get; set; }
    public ImageProperties ImageProperties { get; set; }
    public TextProperties TextProperties { get; set; }
    public Dictionary<string, string> Vars { get; set; }
}

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
[SuppressMessage("ReSharper", "InconsistentNaming")]
public class ConfigParameters {
    public string DataDir { get; set; }
    public string CardArtDir { get; set; }
    public string OutputDir { get; set; }
    public string CropImage { get; set; }
    public Format OutputFormat { get; set; }
    public int TargetDPI { get; set; }
    public int TargetMargin { get; set; }
    public bool Debug { get; set; }
}

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
[SuppressMessage("ReSharper", "InconsistentNaming")]
public class  ScryfallProperties {
    public string FindSetNameURL { get; set; }
    public string FindSetNumURL { get; set; }
}

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class ImageProperties {
    public bool Enabled { get; set; }
    public string Command { get; set; }
    public string Params { get; set; }
    public int Scale { get; set; }
    public float Opacity { get; set; }
}

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
[SuppressMessage("ReSharper", "CollectionNeverUpdated.Global")]
public class TextProperties {
    public string RegularFont { get; set; }
    public string ItalicFont { get; set; }
    public string BoldFont { get; set; }
    public string[] Keywords { get; set; }
    public Dictionary<string, SymbolAdjust> SymbolAdjusts { get; set; }
    public float SymbolMarginRatio { get; set; }
    public Dictionary<string, Dictionary<string, string>> Replacements { get; set; }
}

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class SymbolAdjust {
    public bool Default { get; set; }
    public float Top { get; set; }
    public float Size { get; set; }
    public string[]? Values { get; set; }
}

#pragma warning restore CS8618

public enum Format { Pdf, Png, Jpg }