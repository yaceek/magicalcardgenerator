﻿using System.Diagnostics;

namespace CardGenerator2.Utils;

internal static class ProcessUtil {
    public static void RunProcess(string path, string parameters) {
        var si = new ProcessStartInfo {
            FileName = path,
            Arguments = parameters,
            CreateNoWindow = true,
            UseShellExecute = false,
            RedirectStandardOutput = true,
            RedirectStandardError = true,
            WindowStyle = ProcessWindowStyle.Hidden
        };
        using var proc = Process.Start(si);
        if (proc != null) {
            proc.WaitForExit();
        }
        else {
            throw new Exception("External processing error!");
        }
    }
}
