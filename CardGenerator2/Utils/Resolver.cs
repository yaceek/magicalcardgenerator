﻿using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;
using CardGenerator2.Model.Renderer;
using Microsoft.CodeAnalysis.CSharp.Scripting;
using Microsoft.CodeAnalysis.Scripting;

namespace CardGenerator2.Utils;

[SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class Data {
    public CardFace CardFace { get; }
    public Dictionary<string, string> Vars { get; }

    public Data(CardFace cardface) {
        CardFace = cardface;
        Vars = cardface.Vars;
    }
}

internal static partial class Resolver {
    private static readonly ScriptOptions Opts = ScriptOptions.Default.WithReferences(System.Reflection.Assembly.Load("System.Linq")).WithImports("System", "System.Linq");

    [GeneratedRegex("\\$(?<!\\\\)\\{(.*?)(?<!\\\\)\\}")]
    private static partial Regex ExpressionRegex();

    public static bool EvaluateBoolean(string? value, CardFace cardface) {
        try {
            if (value == null) {
                return false;
            }
            var unescaped = value.Replace("'", "\"");
            var result = CSharpScript.RunAsync(unescaped, Opts, new Data(cardface)).Result;
            return (bool) result.ReturnValue;
        }
        catch (Exception ex) {
            throw new Exception($"failed to evaluate expression '{value}' with message '{ex.Message}'", ex);
        }
    }

    public static string EvaluateString(string? value, CardFace cardface) {
        try {
            if (value == null) {
                return "";
            }
            var keys = ExpressionRegex().Matches(value).ToList().ConvertAll(m => m.Groups[1].Value).Distinct();
            foreach (var key in keys) {
                var unescaped = key.Replace("\\{", "{").Replace("\\}", "}").Replace("'", "\"");
                    var res = CSharpScript.RunAsync(unescaped, Opts, new Data(cardface)).Result;
                    value = value.Replace($"${{{key}}}", res.ReturnValue?.ToString() ?? "");
            }
            return value;
        }
        catch (Exception ex) {
            throw new Exception($"failed to evaluate expression '{value}' with message '{ex.Message}'", ex);
        }
    }

    public static T EvaluateObject<T>(string value, CardFace cardface) {
        try {
            var unescaped = value.Replace("'", "\"");
            var result = CSharpScript.RunAsync(unescaped, Opts, new Data(cardface)).Result;
            return (T)result.ReturnValue;
        }
        catch (Exception ex) {
            throw new Exception($"failed to evaluate expression '{value}' with message '{ex.Message}'", ex);
        }
    }
}