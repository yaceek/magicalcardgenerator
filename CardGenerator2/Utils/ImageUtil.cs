﻿using System.Diagnostics.CodeAnalysis;
using CardGenerator2.Services;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;

namespace CardGenerator2.Utils;

internal static class ImageUtil {
    public static Image LoadImageSimple(string path, int? width = null, int? height = null) {
        var image = Image.Load(path);
        if (width != null || height != null) {
            image.Mutate(img => img.Resize(width ?? image.Width, height ?? image.Height));
        }
        return image;
    }

    public static Image LoadImage(string url, bool process) {
        using var stream = ReadFile(url);
        if (ConfigurationService.Instance.Config.ImageProperties.Enabled && process) {
            var source = Path.Combine(Path.GetTempPath(), $"card_source_{DateTime.Now.Ticks}.bin");
            var processed = Path.Combine(Path.GetTempPath(), $"card_processed_{DateTime.Now.Ticks}.png");
            var combined = Path.Combine(Path.GetTempPath(), $"card_combined_{DateTime.Now.Ticks}.png");
            SaveStream(stream, source);
            Preprocess(source, processed);
            Combine(processed, source, combined);
            var img = Image.Load(combined);
            File.Delete(source);
            File.Delete(processed);
            File.Delete(combined);
            return img;
        }
        return Image.Load(stream);
    }

    private static void SaveStream(Stream stream, string path) {
        using var os = File.OpenWrite(path);
        stream.CopyTo(os);
        os.Close();
    }

    private static void Preprocess(string input, string output) {
        ProcessUtil.RunProcess(ConfigurationService.Instance.Config.ImageProperties.Command, ConfigurationService.Instance.Config.ImageProperties.Params.Replace("${input}", input).Replace("${output}", output).Replace("${scale}", $"{ConfigurationService.Instance.Config.ImageProperties.Scale}"));
    }

    [SuppressMessage("ReSharper", "AccessToDisposedClosure")]
    private static void Combine(string input1, string input2, string output) {
        using var im1 = Image.Load(input1);
        using var im2 = Image.Load(input2);
        im2.Mutate(img => img.Resize(im1.Width, im1.Height));
        im1.Mutate(img => img.DrawImage(im2, ConfigurationService.Instance.Config.ImageProperties.Opacity));
        im1.Save(output);
    }

    private static Stream ReadFile(string path) {
        if (path.StartsWith("http://") || path.StartsWith("https://")) {
            using var client = new HttpClient();
            return client.GetStreamAsync(path).Result;
        }
        return File.OpenRead(path);
    }
}
