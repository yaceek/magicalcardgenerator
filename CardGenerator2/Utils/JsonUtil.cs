﻿using System.Text.Json;
using System.Text.Json.Nodes;
using System.Text.Json.Serialization;

namespace CardGenerator2.Utils;

public static class JsonUtil {
    public static T Load<T>(string path) {
        return JsonSerializer.Deserialize<T>(File.ReadAllText(path), new JsonSerializerOptions {Converters = {new JsonStringEnumConverter(JsonNamingPolicy.CamelCase)}})!;
    }

    public static JsonArray Arr(JsonNode? obj) {
        return obj!.AsArray();
    }

    public static bool Contains(JsonNode? node, params string[] values) {
        return node switch {
            JsonArray arr => arr.Any(elem => values.Any(v => Equals(elem, v))),
            JsonValue value => values.Any(v => value.ToString().Contains(v)),
            _ => false
        };
    }

    public static bool Equals(JsonNode? node, string val) {
        return string.Equals(node?.ToString(), val);
    }

    public static string String(JsonNode? node) {
        return node?.ToString() ?? "";
    }

    public static JsonNode? Traverse(JsonNode? node, params string[] parts) {
        return parts.Aggregate(node, (current, part) => current?[part]);
    }
}
