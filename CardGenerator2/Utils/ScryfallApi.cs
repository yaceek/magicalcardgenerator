﻿using System.Text.Json.Nodes;
using CardGenerator2.Services;

namespace CardGenerator2.Utils;

internal static class ScryfallApi {
    public static JsonNode? FindCardByName(string set, string name) {
        try {
            using var client = new HttpClient();
            var url = ConfigurationService.Instance.Config.ScryfallProperties.FindSetNameURL.Replace("${set}", set).Replace("${name}", name.Replace(" ", "%20"));
            return JsonNode.Parse(client.GetStringAsync(url).Result);
        }
        catch (Exception) {
            return null;
        }
    }

    public static JsonNode? FindCardByNumber(string set, string num) {
        try {
            using var client = new HttpClient();
            var url = ConfigurationService.Instance.Config.ScryfallProperties.FindSetNumURL.Replace("${set}", set).Replace("${num}", num.TrimStart('0'));
            return JsonNode.Parse(client.GetStringAsync(url).Result);
        }
        catch (Exception) {
            return null;
        }
    }

    public static JsonNode? FindCardByUrl(string url) {
        try {
            using var client = new HttpClient();
            return JsonNode.Parse(client.GetStringAsync(url).Result);
        }
        catch (Exception) {
            return null;
        }
    }
}