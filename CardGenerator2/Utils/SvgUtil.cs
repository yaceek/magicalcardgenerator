﻿using System.Drawing.Imaging;
using SixLabors.ImageSharp;
using Svg;

namespace CardGenerator2.Utils;

internal static class SvgUtil {
    public static SvgDocument LoadSvg(string path) {
        return SvgDocument.Open(path);
    }

    public static Image DrawSvg(SvgDocument svg, int height) {
        using var stream = new MemoryStream();
        svg.Draw(0, height).Save(stream, ImageFormat.Png);
        stream.Seek(0, SeekOrigin.Begin);
        return Image.Load(stream);
    }

    public static Image DrawSvg(string path, int height) {
        return DrawSvg(LoadSvg(path), height);
    }
}
