﻿using CardGenerator2.Model.Config;
using CardGenerator2.Services;
using SixLabors.ImageSharp;

namespace CardGenerator2.Output;

internal class ImageOutput : Output {
    public override void StoreImage(Image image, string descriptor, int? count) {
        switch (ConfigurationService.Instance.Config.ConfigParameters.OutputFormat) {
            case Format.Png:
                image.SaveAsPng(Path.Combine(ConfigurationService.Instance.Config.ConfigParameters.OutputDir, $"{descriptor}.png"));
                break;
            case Format.Jpg:
                image.SaveAsJpeg(Path.Combine(ConfigurationService.Instance.Config.ConfigParameters.OutputDir, $"{descriptor}.jpg"));
                break;
            default:
                throw new Exception("Invalid format!");
        }
    }

    public override void Done() { }
}
