﻿using CardGenerator2.Services;
using iText.IO.Image;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;
using Point = SixLabors.ImageSharp.Point;
using Rectangle = iText.Kernel.Geom.Rectangle;

namespace CardGenerator2.Output;

internal class PdfOutput : Output {
    private readonly Image _crop;
    private readonly PdfDocument _pdf;
    private PdfPage? _page;
    private PdfCanvas? _canvas;
    private int _counter;

    public PdfOutput() {
        _pdf = OpenPdfForWriting(System.IO.Path.Combine(ConfigurationService.Instance.Config.ConfigParameters.OutputDir, $"cards_{DateTime.Now:yyyyMMddHHmmss}.pdf"));
        _pdf.SetDefaultPageSize(PageSize.A4);
        _crop = Image.Load(System.IO.Path.Combine(ConfigurationService.Instance.Config.ConfigParameters.DataDir, ConfigurationService.Instance.Config.ConfigParameters.CropImage));
        _counter = 0;
    }

    public override void StoreImage(Image image, string descriptor, int? count) {
        var processed = PrepareImage(image);
        for (var i = 0; i < (count ?? 1); i++) {
            if (_counter % 8 == 0) {
                _canvas?.Release();
                _page = _pdf.AddNewPage(PageSize.A4);
                _canvas = new PdfCanvas(_page);
            }
            DrawOnCanvas(processed);
            _counter++;
        }
    }

    public override void Done() {
        _pdf.Close();
    }

    private void DrawOnCanvas(ImageData image) {
        var w = ReCalc(image.GetWidth());
        var h = ReCalc(image.GetHeight());
        var dx = _counter % 2 == 0 ? -(w + 1) : 1;
        var dy = (_counter / 2 - 2) * (h + 2);

        var pr = _page!.GetPageSize();
        pr.GetWidth();
        pr.GetHeight();

        var rect = new Rectangle(pr.GetWidth() / 2 + dx, pr.GetHeight() / 2 + dy, w, h);
        _canvas?.AddImageFittedIntoRectangle(image, rect, false);
    }

    private ImageData PrepareImage(Image image) {
        image.Mutate(img => {
            img.DrawImage(_crop, Point.Empty, 1.0f);
            img.Rotate(RotateMode.Rotate90);
        });
        using var stream = new MemoryStream();
        image.SaveAsPng(stream);

        return ImageDataFactory.Create(stream.ToArray());
    }

    private static float ReCalc(float val) {
        return val * 72f / ConfigurationService.Instance.Config.ConfigParameters.TargetDPI;
    }

    private static PdfDocument OpenPdfForWriting(string path) {
        return new PdfDocument(new PdfWriter(path, PrepareWriterProperties()));
    }

    private static WriterProperties PrepareWriterProperties() {
        return new WriterProperties().SetPdfVersion(PdfVersion.PDF_1_7).UseSmartMode().SetCompressionLevel(CompressionConstants.DEFAULT_COMPRESSION);
    }
}
