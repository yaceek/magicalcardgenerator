﻿using SixLabors.ImageSharp;

namespace CardGenerator2.Output;

internal abstract class Output {
    public abstract void StoreImage(Image image, string descriptor, int? count);
    public abstract void Done();
}
