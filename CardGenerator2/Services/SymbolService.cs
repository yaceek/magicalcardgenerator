﻿using CardGenerator2.Utils;
using SixLabors.ImageSharp;
using Svg;

namespace CardGenerator2.Services;

internal class SymbolService {
    private readonly Dictionary<string, SvgDocument> _symbols = new();
    // TODO make it a LRU cache with limited size
    private readonly Dictionary<string, Image> _renders = new();

    private static readonly Lazy<SymbolService> Lazy = new(() => new SymbolService());
    public static SymbolService Instance => Lazy.Value;

    private SymbolService() {
        Directory.GetFiles(Path.Combine(ConfigurationService.Instance.Config.ConfigParameters.DataDir, "symbols"), "*.svg").ToList().ForEach(f => {
            _symbols[Path.GetFileNameWithoutExtension(f)] = SvgUtil.LoadSvg(f);
        });
    }

    public int CalculateWidth(string name, int height) {
        return (int)(_symbols[name].Width.Value * height / _symbols[name].Height.Value);
    }

    // TODO synchronize when multithreading implemented!
    public Image GetSymbol(string name, int height) {
        var key = $"{name}/{height}";
        if (!_renders.ContainsKey(key)) {
            _renders[key] = SvgUtil.DrawSvg(_symbols[name], height);
        }
        return _renders[key];
    }
}
