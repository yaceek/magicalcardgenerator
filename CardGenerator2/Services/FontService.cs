﻿using SixLabors.Fonts;

namespace CardGenerator2.Services;

internal class FontService {
    private readonly FontCollection _collection = new();
    // TODO make it a LRU cache with limited size
    private readonly Dictionary<string, Font> _fonts = new();

    private static readonly Lazy<FontService> Lazy = new(() => new FontService());
    public static FontService Instance => Lazy.Value;

    private FontService() {
        Directory.GetFiles(Path.Combine(ConfigurationService.Instance.Config.ConfigParameters.DataDir, "fonts"), "*.ttf").ToList().ForEach(f => {
            _collection.Add(f);
        });
    }

    // TODO synchronize when multithreading implemented!
    public Font GetFont(string name, int size) {
        var key = $"{name}/{size}";
        if(!_fonts.ContainsKey(key)) {
            var parts = name.Split('|');
            if (parts.Length > 1) {
                var style = parts[1].ToLower() switch {
                    "bold" => FontStyle.Bold,
                    "italic" => FontStyle.Italic,
                    "bold_italic" => FontStyle.BoldItalic,
                    _ => FontStyle.Regular
                };
                _fonts[key] = _collection.Get(parts[0]).CreateFont(size, style);
            }
            else {
                _fonts[key] = _collection.Get(name).CreateFont(size);
            }
        }
        return _fonts[key];
    }

    // TODO synchronize when multithreading implemented!
    public (Font font, string name, int ascender, int descender, int linegap, int height, int lineheight) GetFontDetail(string name, int size) {
        var font = GetFont(name, size);
        var asc = CalculateAscender(font);
        var dsc =  CalculateDescender(font);
        var gap = CalculateLineGap(font);
        return (font, name, (int)asc, (int)dsc, (int)gap, (int)(asc + dsc), (int)(asc + dsc + gap));
    }

    private static float CalculateAscender(Font font) {
        return Math.Abs(font.Size * font.FontMetrics.Ascender / font.FontMetrics.UnitsPerEm);
    }

    private static float CalculateDescender(Font font) {
        return Math.Abs(font.Size * font.FontMetrics.Descender / font.FontMetrics.UnitsPerEm);
    }

    private static float CalculateLineGap(Font font) {
        return Math.Abs(font.Size * font.FontMetrics.LineGap / font.FontMetrics.UnitsPerEm);
    }
}
