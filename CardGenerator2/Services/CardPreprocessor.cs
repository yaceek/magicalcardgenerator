﻿using System.Text.Json.Nodes;
using System.Text.RegularExpressions;
using CardGenerator2.Model.Config;
using CardGenerator2.Model.Renderer;
using CardGenerator2.Utils;

namespace CardGenerator2.Services;

internal static class CardPreprocessor {
    public static List<CardFace> PreprocessCard(JsonObject card, Dictionary<string, string?> overrides) {
        // initialize frame effects array if it is missing
        if (!card.ContainsKey("frame_effects")) {
            card["frame_effects"] = new JsonArray();
        }
        // initialize keywords array if it is missing
        if (!card.ContainsKey("keywords")) {
            card["keywords"] = new JsonArray();
        }
        // initialize meld result
        if (JsonUtil.Equals(card["layout"], "meld")) {
            foreach (var node in card["all_parts"]!.AsArray()) {
                if (JsonUtil.Equals(node!["component"], "meld_result")) {
                    card["meld_result"] = ScryfallApi.FindCardByUrl(node["uri"]!.ToString());
                    break;
                }
            }
        }

        // 'fix' conflux set symbol/watermark
        if (JsonUtil.Equals(card["set"], "con")) {
            card["set_symbol"] = "con_";
        }
        if (JsonUtil.Equals(card["watermark"], "con")) {
            card["watermark"] = "con_";
        }

        // apply overrides
        foreach (var (key, value) in overrides) {
            var split = key.Split('|', StringSplitOptions.TrimEntries);
            if (split.Length > 1 && card.ContainsKey("card_faces") && (split[0].Equals("front") || split[0].Equals("back"))) {
                card["card_faces"]![split[0].Equals("front") ? 0 : 1]![split[1]] = value;
            }
            else if(split.Length == 1) {
                card[split[0]] = value;
            }
        }

        // MID transform cards do not have the proper 'frame_effect' configured
        if (JsonUtil.Equals(card["layout"], "transform") && JsonUtil.Equals(card["set"], "mid") && JsonUtil.Contains(card["keywords"], "Transform", "Daybound", "Disturb")) {
            JsonUtil.Arr(card["frame_effects"]).Add("sunmoondfc");
        }
        // NEO transform cards do not have the proper 'frame_effect' configured
        if (JsonUtil.Equals(card["layout"], "transform") && JsonUtil.Equals(card["set"], "neo") && JsonUtil.Contains(card["keywords"], "Transform")) {
            JsonUtil.Arr(card["frame_effects"]).Add("fandfc");
        }
        // BRO meld cards do not have the proper 'frame_effect' configured
        if (JsonUtil.Equals(card["layout"], "meld") && JsonUtil.Equals(card["set"], "bro")) {
            JsonUtil.Arr(card["frame_effects"]).Add("hammerdfc");
        }
        // old-frame cards with flashback have a tombstone in the corner
        if (JsonUtil.Contains(card["keywords"], "Flashback")) {
            JsonUtil.Arr(card["frame_effects"]).Add("flashback");
        }

        // fill set size
        card["set_size"] = ConfigurationService.Instance.Config.Vars[$"set.{card["set"]}.size"];

        // fuse cards have fuse text in oracle
        if (JsonUtil.Contains(card["keywords"],"Fuse")) {
            RemoveOracle(card["card_faces"]![0]!.AsObject(), "\nFuse");
            RemoveOracle(card["card_faces"]![1]!.AsObject(), "\nFuse");
        }

        // non-breaking 'the X'
        if (overrides.ContainsKey("keep_the")) {
            if (card.ContainsKey("card_faces")) {
                KeepTheOracle(card["card_faces"]![0]!.AsObject());
                KeepTheOracle(card["card_faces"]![1]!.AsObject());
            }
            else {
                KeepTheOracle(card);
            }
        }

        // modal land cards do not have 'mana_produced' property configured
        // fetch land cards do not have 'mana_produced' property configured
        if (JsonUtil.Equals(card["layout"], "modal_dfc") || JsonUtil.Equals(card["layout"],"transform")) {
            FixManaProduced(card["card_faces"]![0]!.AsObject());
            FixManaProduced(card["card_faces"]![1]!.AsObject());
        }
        else {
            FixManaProduced(card);
        }

        // apply additional replacements
        ApplyReplacements(card);

        var cardfaces = new List<CardFace>();
        if (card.ContainsKey("card_faces")) {
            var f = new CardFace(card, "front", LoadLayout(card, card["card_faces"]![0]!.AsObject()));
            var b = new CardFace(card, "back", LoadLayout(card, card["card_faces"]![1]!.AsObject()));
            f.OtherSide = b;
            b.OtherSide = f;
            cardfaces.Add(f);
            if (JsonUtil.Equals(card["layout"], "modal_dfc") || JsonUtil.Equals(card["layout"],"transform")) {
                cardfaces.Add(b);
            }
        }
        else {
            cardfaces.Add(new CardFace(card, "front", LoadLayout(card)));
        }

        return cardfaces;
    }

    private static Layout LoadLayout(JsonNode card, JsonNode? face = null) {
        string name;
        if (JsonUtil.Contains(face != null ? face["type_line"] : card["type_line"], "Saga")) {
            name = JsonUtil.Equals(card["layout"], "saga") ? "normal-saga" : $"{card["layout"]}-saga";
        }
        else if (JsonUtil.Contains(face != null ? face["type_line"] : card["type_line"], "Class")) {
            name = JsonUtil.Equals(card["layout"], "class") ? "normal-class" : $"{card["layout"]}-class";
        }
        else if (JsonUtil.Contains(face != null ? face["type_line"] : card["type_line"], "Planeswalker")) {
            name = $"{card["layout"]}-planeswalker{(JsonUtil.String(face != null ? face["oracle_text"] : card["oracle_text"]).Split('\n').Length > 3 ? 4 : 3)}";
        }
        else if (JsonUtil.Equals(card["layout"], "leveler") || JsonUtil.Equals(card["layout"], "adventure") || JsonUtil.Equals(card["layout"], "flip")) {
            name = JsonUtil.String(card["layout"]);
        }
        else if (JsonUtil.Equals(card["layout"], "split") && JsonUtil.Contains(card["keywords"], "Aftermath")) {
            name = $"{card["layout"]}-aftermath";
        }
        else if (JsonUtil.Equals(card["layout"], "split") && JsonUtil.Contains(card["keywords"], "Fuse")) {
            name = $"{card["layout"]}-fuse";
        }
        else if (JsonUtil.Equals(card["layout"], "normal") && JsonUtil.Contains(card["keywords"], "Mutate")) {
            name = $"{card["layout"]}-mutate";
        }
        else if (JsonUtil.Equals(card["layout"], "normal") && JsonUtil.Contains(card["keywords"], "Prototype")) {
            name = $"{card["layout"]}-prototype";
        }
        else {
            name = $"{card["layout"]}-standard";
        }

        var dir = $"{ConfigurationService.Instance.Config.ConfigParameters.DataDir}/frames/{card["frame"]}/{name}";
        var layout = JsonUtil.Load<Layout>(Path.Combine(dir, "layout.json"));
        if (!string.IsNullOrEmpty(layout.Parent)) {
            var parent = JsonUtil.Load<Parent>(Path.Combine(dir, layout.Parent));
            foreach (var (k, v) in parent.Vars) {
                // do not override variables with the ones taken from parent
                if (!layout.Vars.ContainsKey(k)) {
                    layout.Vars[k] = v;
                }
            }
            if (parent.Spans.Length > 0) {
                layout.Spans = parent.Spans.Concat(layout.Spans).ToArray();
            }
        }
        layout.Dir = dir;
        return layout;
    }

    private static void ApplyReplacements(JsonObject card) {
        foreach (var (field, replacements) in ConfigurationService.Instance.Config.TextProperties.Replacements) {
            ApplyReplacement(card, field, replacements);
            if (card.ContainsKey("card_faces")) {
                var arr = JsonUtil.Arr(card["card_faces"]);
                foreach (var elem in arr) {
                    ApplyReplacement(elem!.AsObject(), field, replacements);
                }
            }
        }
    }

    private static void ApplyReplacement(JsonNode node, string field, Dictionary<string, string> replacements) {
        var text = JsonUtil.String(node[field]);
        foreach (var (key, val) in replacements) {
            text = text.Replace(key, val);
        }
        node[field] = text;
    }

    private static void RemoveOracle(JsonNode face, string str) {
        var txt = JsonUtil.String(face["oracle_text"]);
        var idx = txt.IndexOf(str, StringComparison.InvariantCulture);
        face["oracle_text"] = txt[..idx];
    }

    private static void KeepTheOracle(JsonNode face) {
        var txt = JsonUtil.String(face["oracle_text"]);
        face["oracle_text"] = txt.Replace(" the ", " the\u00A0");
    }

    private static void FixManaProduced(JsonObject face) {
        if (!face.ContainsKey("produced_mana")) {
            face["produced_mana"] = new JsonArray();
        }
        if (JsonUtil.Contains(face["type_line"], "Land") && face["oracle_text"] != null && face["produced_mana"]!.AsArray().Count == 0) {
            var oracle = JsonUtil.String(face["oracle_text"]);
            var arr = JsonUtil.Arr(face["produced_mana"]);
            if (oracle.Contains("Add {W}.")) {
                arr.Add("W");
            }
            if (oracle.Contains("Add {U}.")) {
                arr.Add("U");
            }
            if (oracle.Contains("Add {B}.")) {
                arr.Add("B");
            }
            if (oracle.Contains("Add {R}.")) {
                arr.Add("R");
            }
            if (oracle.Contains("Add {G}.")) {
                arr.Add("G");
            }
            if (oracle.Contains("Search your library for a basic land card") || oracle.Contains("choose a basic land type")) {
                arr.Add("W");
                arr.Add("U");
                arr.Add("B");
                arr.Add("R");
                arr.Add("G");
            }
            if (Regex.IsMatch(oracle, "Search your library for a .*Plains.* card")) {
                arr.Add("W");
            }
            if (Regex.IsMatch(oracle, "Search your library for a .*Island.* card")) {
                arr.Add("U");
            }
            if (Regex.IsMatch(oracle, "Search your library for a .*Swamp.* card")) {
                arr.Add("B");
            }
            if (Regex.IsMatch(oracle, "Search your library for a .*Mountain.* card")) {
                arr.Add("R");
            }
            if (Regex.IsMatch(oracle, "Search your library for a .*Forest.* card")) {
                arr.Add("G");
            }
        }
    }
}