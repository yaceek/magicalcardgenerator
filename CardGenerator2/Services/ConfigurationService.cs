﻿using CardGenerator2.Model.Config;
using CardGenerator2.Utils;

namespace CardGenerator2.Services;

internal class ConfigurationService {
    private static readonly Lazy<ConfigurationService> Lazy = new(() => new ConfigurationService());
    public static ConfigurationService Instance => Lazy.Value;

    public Configuration Config { get; }

    private ConfigurationService() {
        Config = JsonUtil.Load<Configuration>("config.json");
    }
}
