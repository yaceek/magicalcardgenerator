﻿using System.Text.RegularExpressions;
using CardGenerator2.Model.Text;
using SixLabors.ImageSharp;

namespace CardGenerator2.Services;

public static partial class TextProcessor {
    [GeneratedRegex("^[{}\\dWUBRG]+: Level \\d")]
    private static partial Regex ClassRegex();

    [GeneratedRegex("^[+−]?[X\\d]+:.*")]
    private static partial Regex PlaneswalkerRegex();

    [GeneratedRegex("^[IV, ]+ —.*")]
    private static partial Regex SagaRegex();

    [GeneratedRegex("^(Prototype) ([{}\\dWUBRG]+) — (\\d+/\\d+) (.*)")]
    private static partial Regex PrototypeRegex();

    public static List<Section> SplitTextStandard(string oracle, string flavor, bool remove) {
        var sections = new List<Section>();
        if (remove) {
            while (oracle.Contains('(')) {
                var i1 = oracle.IndexOf("(", StringComparison.Ordinal);
                var i2 = oracle.IndexOf(")", StringComparison.Ordinal);
                if (i1 >= 0 && i2 > i1) {
                    oracle = oracle.Remove(i1, i2 - i1 + 1);
                }
            }
        }
        if (oracle.Length > 0) {
            var paragraphs = SplitParagraphs(oracle, false);
            var bg = true;
            foreach (var p in paragraphs.Where(p => p.Background)) {
                p.Background = bg; bg = !bg;
            }
            sections.Add(new Section(0, "oracle", paragraphs));
        }
        if (flavor.Length > 0) {
            sections.Add(new Section(1, "flavor", SplitParagraphs(flavor, true)));
        }
        return sections;
    }

    public static List<Section> SplitTextMutate(string oracle, bool remove) {
        var sections = new List<Section>();
        if (remove) {
            while (oracle.Contains('(')) {
                var i1 = oracle.IndexOf("(", StringComparison.Ordinal);
                var i2 = oracle.IndexOf(")", StringComparison.Ordinal);
                if (i1 >= 0 && i2 > i1) {
                    oracle = oracle.Remove(i1, i2 - i1 + 1);
                }
            }
        }
        if (oracle.Length > 0) {
            var paragraphs = SplitParagraphs(oracle, false);
            sections.Add(new Section(0, "mutate", paragraphs.GetRange(0, 1)));
            sections.Add(new Section(1, "rules", paragraphs.GetRange(1, paragraphs.Count - 1)));
        }
        return sections;
    }

    public static List<Section> SplitTextPrototype(string oracle, string flavor, bool remove) {
        var sections = new List<Section>();
        if (remove) {
            while (oracle.Contains('(')) {
                var i1 = oracle.IndexOf("(", StringComparison.Ordinal);
                var i2 = oracle.IndexOf(")", StringComparison.Ordinal);
                if (i1 >= 0 && i2 > i1) {
                    oracle = oracle.Remove(i1, i2 - i1 + 1);
                }
            }
        }
        if (oracle.Length > 0) {
            var match = PrototypeRegex().Match(oracle);
            var paragraphs = SplitParagraphs(  PrototypeRegex().Replace(oracle, "$1 $4"), false);
            sections.Add(new Section(0, "proto", paragraphs.GetRange(0, 1)) { Label = match.Groups[2].Value, Extra = match.Groups[3].Value});
            sections.Add(new Section(1, "rules", paragraphs.GetRange(1, paragraphs.Count - 1)));
        }
        if (flavor.Length > 0) {
            sections.Add(new Section(2, "flavor", SplitParagraphs(flavor, true)));
        }
        return sections;
    }

    public static List<Section> SplitTextSaga(string text) {
        var sections = new List<Section>();
        var idx = 0;
        foreach (var paragraph in text.Split('\n', StringSplitOptions.TrimEntries)) {
            if (SagaRegex().IsMatch(paragraph)) {
                var i = paragraph.IndexOf("—", StringComparison.Ordinal);
                // TODO label/key as array?
                var key = paragraph[..(i - 1)];
                sections.Add(new Section(idx++, key, new List<Paragraph> { ProcessParagraph(paragraph[(i + 2)..], false) }) { Indent = true, Label = key });
            }
            else {
                sections.Add(new Section(idx++, "_", new List<Paragraph> { ProcessParagraph(paragraph, true) }) { Label = "reminder"});
            }
        }
        return sections;
    }

    public static List<Section> SplitTextPlaneswalker(string text) {
        var sections = new List<Section>();
        Section? previous = null;
        var idx = 0;
        foreach (var paragraph in text.Split('\n', StringSplitOptions.TrimEntries)) {
            if (PlaneswalkerRegex().IsMatch(paragraph)) {
                var i = paragraph.IndexOf(":", StringComparison.Ordinal);
                var label = paragraph[..i];
                var key = label.Equals("0") ? "0" : label.StartsWith("+") ? "+" : "-";
                sections.Add(new Section(idx++, key, new List<Paragraph> { ProcessParagraph(paragraph[(i + 2)..], false) }) { Indent = true, Label = label });
                previous = null;
            }
            else {
                if (previous == null) {
                    previous = new Section(idx++, "_", new List<Paragraph> {ProcessParagraph(paragraph, false)});
                    sections.Add(previous);
                }
                else {
                    previous.Paragraphs.Add(ProcessParagraph(paragraph, false));
                }
            }
        }
        return sections;
    }

    public static List<Section> SplitTextClass(string text) {
        var sections = new List<Section>();
        Section? previous = null;
        var paragraphs = text.Split('\n', StringSplitOptions.TrimEntries);
        var idx = 0;
        sections.Add(new Section(idx++, "_", new List<Paragraph> { ProcessParagraph(paragraphs[0], true) }) { Label = "reminder" });
        foreach (var paragraph in paragraphs.Skip(1)) {
            if (ClassRegex().IsMatch(paragraph)) {
                var i = paragraph.IndexOf(":", StringComparison.Ordinal);
                var label = paragraph[..i];
                var key = paragraph[(i + 2)..];
                previous = new Section(idx++, key, new List<Paragraph>()) { Label = label };
                sections.Add(previous);
            }
            else {
                if (previous == null) {
                    previous = new Section(idx++, "_", new List<Paragraph> { ProcessParagraph(paragraph, false) });
                    sections.Add(previous);
                }
                else {
                    previous.Paragraphs.Add(ProcessParagraph(paragraph, false));
                }
            }
        }
        return sections;
    }

    public static List<Section> SplitTextLeveler(string oracle) {
        var sections = new List<Section>();
        var levels = oracle.Split("LEVEL", StringSplitOptions.TrimEntries);
        // first
        sections.Add(new Section(0, "_", new List<Paragraph> {ProcessParagraph(levels[0], false)}));
        // second
        var l2 = levels[1].Split('\n', StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
        sections.Add(new Section(1, "lvl2", l2.Length > 2 ? new List<Paragraph> {ProcessParagraph(l2[2], false)} : new List<Paragraph>()) { Indent = true, Label = l2[0], Extra = l2[1] });
        // third
        var l3 = levels[2].Split('\n', StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
        sections.Add(new Section(2, "lvl3", l3.Length > 2 ? l3[2..].Select(str => ProcessParagraph(str, false)).ToList() : new List<Paragraph>()) { Indent = true, Label = l3[0], Extra = l3[1] });
        return sections;
    }

    private static List<Paragraph> SplitParagraphs(string text, bool italic) {
        return text.Split('\n').Select(str => ProcessParagraph(str, italic)).Where(paragraph => paragraph.Words.Count > 0).ToList();
    }

    public static Paragraph ProcessParagraph(string text, bool italic, Color? color = null) {
        text = FixQuotes(text);

        var bracketed = false;
        var bold = text.Contains('|');
        var keyword = IsKeyword(text.Contains('|') ? text[(text.IndexOf('|') + 1)..].Trim() : text.StartsWith('•') ? text[1..].Trim() : text);

        var paragraph = new Paragraph { Background = bold };
        foreach (var str in text.Split(' ', StringSplitOptions.RemoveEmptyEntries)) {
            if (str.Contains('(')) {
                bracketed = true;
            }
            if (str.Contains('|')) {
                bold = false;
            }
            if (str.Contains('—')) {
                keyword = false;
            }

            var s = str.Replace("*", "").Replace("#", "*");
            // TODO this is a hack for AFR dice roller cards
            if (bold) {
                s = s.Replace("—", "-");
            }

            // TODO '*' is for toggling italics... would need to go char-by-char to process it properly
            // TODO '#' -> '*' is a hack for P/T
            paragraph.Words.Add(ProcessWord(s, !bold && (bracketed || keyword || italic), bold, color));
            if (str.Contains(')')) {
                bracketed = false;
            }
        }
        return paragraph;
    }

    private static Word ProcessWord(string text, bool italic, bool bold, Color? color = null) {
        var word = new Word();
        while (text.Length > 0) {
            if (text.StartsWith("{")) {
                var i = text.IndexOf("}", StringComparison.Ordinal);
                var t = text[1..i].Replace("/", "");
                word.GlyphRuns.Add(new GlyphRun(t, GlyphType.Symbol));
                text = text[(i + 1)..];
            }
            else if (text.Contains('{')) {
                var i = text.IndexOf('{', StringComparison.Ordinal);
                var t = text[..i];
                var run = new GlyphRun(t, italic ? GlyphType.Italic : bold ? GlyphType.Bold : GlyphType.Regular);
                if (color != null) {
                    run.Color = color.Value;
                }
                word.GlyphRuns.Add(run);
                text = text[i..];
            }
            else {
                var run = new GlyphRun(text, italic ? GlyphType.Italic : bold ? GlyphType.Bold : GlyphType.Regular);
                if (color != null) {
                    run.Color = color.Value;
                }
                word.GlyphRuns.Add(run);
                break;
            }
        }
        return word;
    }

    private static bool IsKeyword(string text) {
        return ConfigurationService.Instance.Config.TextProperties.Keywords.Any(text.StartsWith);
    }

    private static string FixQuotes(string text) {
        // replace " with “ and ”
        // replace ' with ‘ and ’
        var arr = text.ToCharArray();
        var dq = false;
        for (var i = 0; i < arr.Length; i++) {
            switch (arr[i]) {
                case '\'':
                    arr[i] = i == 0 || arr[i - 1] == ' ' ? '‘' : '’';
                    break;
                case '"':
                    arr[i] = dq ? '”' : '“';
                    dq = !dq;
                    break;
            }
        }
        return new string(arr);
    }
}