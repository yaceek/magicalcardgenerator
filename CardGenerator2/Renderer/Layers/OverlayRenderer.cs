﻿using CardGenerator2.Model.Config;
using CardGenerator2.Model.Renderer;
using CardGenerator2.Utils;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;

namespace CardGenerator2.Renderer.Layers;

internal class OverlayRenderer : BaseRenderer {
    protected override void RenderInternal(IImageProcessingContext img, Layer? layer, CardFace cardface, string dir) {
        Log.Info("      drawing Overlay");
        using var im = ImageUtil.LoadImage(Resolver.EvaluateString(layer!.Source, cardface), true);
        DrawImage(img, Rectangle.Inflate(cardface.Layout.Dimensions, -cardface.Layout.Meta.Margin, -cardface.Layout.Meta.Margin), false, 0, im);
    }
}
