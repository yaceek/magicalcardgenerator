﻿using System.Diagnostics.CodeAnalysis;
using CardGenerator2.Model.Config;
using CardGenerator2.Model.Renderer;
using CardGenerator2.Model.Text;
using CardGenerator2.Renderer.Layers.Extras;
using CardGenerator2.Services;
using CardGenerator2.Utils;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Drawing.Processing;
using SixLabors.ImageSharp.Processing;

namespace CardGenerator2.Renderer.Layers;

internal class TextRendererVertical : TextRenderer2 {
    protected override void DrawBox(IImageProcessingContext img, TextBox box, CardFace cardface) {
        Log.Info($"    writing vertical textbox {box.Id}");

        // resolve text sections
        var sections = Resolver.EvaluateObject<List<Section>>(box.Source, cardface);
        var indicated = sections.FindAll(s => !s.Key.Equals("_")); // TODO mark sections with indicators in TextProcessor

        // prepare constrains
        var constrains = new Constrains {
            Width = box.Position.Box.Width,
            Height = box.Position.Box.Height - (box.Separators?.Take(..(sections.Count - 1)).Sum(s => s.Height) ?? 0),
            HeightTotal = box.Position.Box.Height
        };

        // prepare indicators
        if (box.Indicator?.Images != null) {
            IndicatorSupport.PrepareIndicators(indicated, box.Indicator, cardface);
        }

        // draw sections
        DrawSections(img, sections, constrains, box, cardface);

        // draw indicators
        if (box.Indicator?.Images != null) {
            IndicatorSupport.DrawIndicators(img, indicated, box.Indicator);
        }
    }

    [SuppressMessage("ReSharper", "AccessToDisposedClosure")]
    private void DrawSections(IImageProcessingContext img, List<Section> sections, Constrains constrains, TextBox box, CardFace cardface) {
        if (sections.Count > 0) {
            PreprocessSections(sections, constrains, box, cardface.FieldInt("textbox_fontsize") ?? box.FontParameters.Size);

            var pos = box.Position.Box.Location;
            var pad = (constrains.Height - sections.Sum(s => s.ContentsImage!.Height)) / ((sections.Count - 1) * 2); // TODO make it configurable when sections are defined in template... not the current 'fixed section' style

            for (var i = 0; i < sections.Count; i++) {
                var section = sections[i];

                if (i > 0 && box.Separators != null) {
                    var separator = box.Separators[i - 1];
                    if (separator.Image != null) {
                        DrawSeparator(img, pos, constrains.Width, separator, cardface);
                    }

                    if (ConfigurationService.Instance.Config.ConfigParameters.Debug) {
                        DrawOutline(img, Color.Red, new Rectangle(pos, new Size(constrains.Width, separator.Height)));
                    }

                    pos.Offset(0, separator.Height);
                    pos.Offset(0, pad);
                }

                // draw section image
                img.DrawImage(section.ContentsImage, pos, 1.0f);
                section.Middle = pos.Y + section.ContentsImage!.Height / 2;
                pos.Offset(0, section.ContentsImage.Height);

                if (i > 0) {
                    pos.Offset(0, pad);
                }
            }
        }

        if (ConfigurationService.Instance.Config.ConfigParameters.Debug) {
            img.Draw(Pens.Solid(Color.Blue, 1), box.Position.Box);
        }
    }

    private void DrawSeparator(IImageProcessingContext img, Point position, int width, Separator separator, CardFace cardface) {
        // load separator image
        using var si = ImageUtil.LoadImageSimple(Path.Combine(cardface.Layout.Dir, separator.Image!), width - (separator.Margins?.Left ?? 0) - (separator.Margins?.Right ?? 0));

        // draw ON the separator image
        DrawOnSeparator(separator, si, cardface);

        // draw the separator image
        var point = separator.Align switch {
            AlignVertical.Center => new Point(position.X + (separator.Margins?.Left ?? 0), position.Y + (separator.Height - si.Height) / 2),
            AlignVertical.Bottom => new Point(position.X + (separator.Margins?.Left ?? 0), position.Y + separator.Height - si.Height),
            _ => new Point(position.X + (separator.Margins?.Left ?? 0), position.Y)
        };
        img.DrawImage(si, point, 1.0f);

        if (ConfigurationService.Instance.Config.ConfigParameters.Debug) {
            DrawOutline(img, Color.Yellow, new Rectangle(point, new Size(si.Width, si.Height)));
            img.DrawLines(Pens.Solid(Color.Magenta, 1), new PointF(point.X, point.Y + si.Height / 2), new PointF(point.X + si.Width, point.Y + si.Height / 2));
        }
    }

    // TODO unify with TextRenderer.DrawSpan
    private void DrawOnSeparator(Separator separator, Image image, CardFace cardface) {
        if (separator.Elements?.Length > 0) {
            var range = (0, image.Width);
            image.Mutate(im => {
                foreach (var elem in separator.Elements) {
                    if (elem.Text != null) {
                        Log.Info("      drawing text element on separator");
                        range = DrawTextElement(im, elem.Text, cardface, range, image.Height, elem.Align);
                    }
                    else if (elem.Img != null) {
                        Log.Info("      drawing svg element on separator");
                        range = DrawImgElement(im, elem.Img, cardface, range, image.Height, elem.Align);
                    }
                }
            });
        }
    }
}
