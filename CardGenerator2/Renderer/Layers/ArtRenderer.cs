﻿using CardGenerator2.Model.Config;
using CardGenerator2.Model.Renderer;
using CardGenerator2.Services;
using CardGenerator2.Utils;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;

namespace CardGenerator2.Renderer.Layers;

internal class ArtRenderer : BaseRenderer {
    protected override void RenderInternal(IImageProcessingContext img, Layer? layer, CardFace cardface, string dir) {
        var card = Resolver.EvaluateObject<CardFace>(layer!.Source, cardface);
        var mask = layer.Mask != null ? Image.Load(Path.Combine(dir, Resolver.EvaluateString(layer.Mask, cardface))) : null;

        var full = card.FieldTrue("full_art");
        var bounds = full ? Rectangle.Inflate(card.Layout.Dimensions, -card.Layout.Meta.Margin, -card.Layout.Meta.Margin) : layer.Position!.Box;
        var rotation = layer.Position!.Rotation;

        if (card.HasField("art_image")) {
            Log.Info($"      drawing {(full ? "full" : "cropped")} art from override");
            using var im = ImageUtil.LoadImage(card.Field("art_image")!, false);
            DrawImage(img, bounds, false, rotation, im, mask);
        }
        else {
            var png = Path.Combine(ConfigurationService.Instance.Config.ConfigParameters.CardArtDir, $"{card.Field("name")};{card.Field("artist")}.png");
            var jpg = Path.Combine(ConfigurationService.Instance.Config.ConfigParameters.CardArtDir, $"{card.Field("name")};{card.Field("artist")}.jpg");
            var path = File.Exists(png) ? png : File.Exists(jpg) ? jpg : null;

            if (path != null) {
                Log.Info($"      drawing {(full ? "full" : "cropped")} art from directory");
                using var im = ImageUtil.LoadImage(path, false);
                DrawImage(img, bounds, false, rotation, im, mask);
            }
            else {
                Log.Info($"      drawing {(full ? "full" : "cropped")} art from scryfall");
                using var im = ImageUtil.LoadImage(card.Field("image_uris|art_crop")!, true);
                DrawImage(img, bounds, false, rotation, im, mask);
            }
        }
    }
}
