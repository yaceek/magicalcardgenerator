﻿using System.Diagnostics.CodeAnalysis;
using CardGenerator2.Model.Config;
using CardGenerator2.Model.Renderer;
using CardGenerator2.Utils;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;

namespace CardGenerator2.Renderer.Layers;

internal class FrameRenderer : BaseRenderer {
    [SuppressMessage("ReSharper", "AccessToDisposedClosure")]
    protected override void RenderInternal(IImageProcessingContext img, Layer? layer, CardFace cardface, string dir) {
        var path = Path.Combine(dir, Resolver.EvaluateString(layer!.Source, cardface));
        Log.Info($"      using image '{path}'");
        using var image = ImageUtil.LoadImage(path, false);

        var bounds = layer.Position?.Box ?? cardface.Layout.Dimensions;
        if (layer.Mask != null) {
            using var imagemask = Image.Load(Path.Combine(dir, Resolver.EvaluateString(layer.Mask, cardface)));
            image.Mutate(im => im.Resize(imagemask.Width, imagemask.Height));
            imagemask.Mutate(im => im.DrawImage(image, new GraphicsOptions { AlphaCompositionMode = PixelAlphaCompositionMode.SrcIn }));
            DrawImage(img, bounds, layer.Fit, 0, imagemask);
        }
        else {
            DrawImage(img, bounds, layer.Fit, 0, image);
        }
    }
}
