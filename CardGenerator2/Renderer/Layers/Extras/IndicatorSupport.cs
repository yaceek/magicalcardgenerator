﻿using CardGenerator2.Model.Config;
using CardGenerator2.Model.Renderer;
using CardGenerator2.Model.Text;
using CardGenerator2.Services;
using CardGenerator2.Utils;
using SixLabors.Fonts;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Drawing.Processing;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;

namespace CardGenerator2.Renderer.Layers.Extras;

internal static class IndicatorSupport {
    public static void PrepareIndicators(List<Section> sections, Indicator indicator, CardFace cardface) {
        var dir = cardface.Layout.Dir;
        foreach (var section in sections) {
            var keys = section.Key.Split(',', StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
            var images = keys.ToList().ConvertAll(key => PrepareIndicatorImage(dir, key, section.Label, indicator, cardface));
            var image = new Image<Rgba32>(images.First().Width, images.Sum(i => i.Height) + indicator.ImageSpacing * (images.Count - 1));
            image.Mutate(im => {
                var point = Point.Empty;
                foreach (var i in images) {
                    im.DrawImage(i, point, 1.0f);
                    point.Offset(0, i.Height + indicator.ImageSpacing);
                }
            });
            section.IndicatorImage = image;
        }
    }

    private static Image PrepareIndicatorImage(string dir, string key, string? label, Indicator extras, CardFace cardface) {
        var image = Image.Load(Path.Combine(dir, extras.Images[key].Path));
        if (extras.Label != null && label != null) {
            var font = FontService.Instance.GetFont(extras.Label.Font.RegularFont!, extras.Label.Font.Size);
            image.Mutate(im => {
                im.DrawText(new TextOptions(font) {
                    Origin = extras.Label.Positions[key],
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center
                }, label, Color.Parse(Resolver.EvaluateString(extras.Label.Font.Color ?? "black", cardface)));
            });
        }
        return image;
    }

    public static void DrawIndicators(IImageProcessingContext img, IEnumerable<Section> sections, Indicator indicator) {
        foreach (var section in sections.Where(section => section.IndicatorImage != null)) {
            DrawIndicator(img, section, indicator);
        }
    }

    private static void DrawIndicator(IImageProcessingContext img, Section section, Indicator indicator) {
        // TODO support 'special' key * == all
        var dy = indicator.Images.TryGetValue(section.Key, out var value) ? value.Adjust : 0;
        var point = new Point(indicator.ImagePosition, section.Middle + dy - section.IndicatorImage!.Height / 2);
        img.DrawImage(section.IndicatorImage, point, 1.0f);

        if (ConfigurationService.Instance.Config.ConfigParameters.Debug) {
            img.Draw(Pens.Solid(Color.Lime, 1), new Rectangle(point.X, point.Y, section.IndicatorImage.Width, section.IndicatorImage.Height));
        }
    }
}