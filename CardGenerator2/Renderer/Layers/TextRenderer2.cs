﻿using CardGenerator2.Model.Config;
using CardGenerator2.Model.Renderer;
using CardGenerator2.Model.Text;
using CardGenerator2.Services;
using CardGenerator2.Utils;
using SixLabors.Fonts;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Drawing.Processing;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using GlyphType = CardGenerator2.Model.Text.GlyphType;

namespace CardGenerator2.Renderer.Layers;

internal class Constrains {
    public int Width { get; init; }
    public int Height { get; init; }
    public int HeightTotal { get; init; }
}

internal abstract class TextRenderer2 : BaseRenderer {
    protected override void RenderInternal(IImageProcessingContext img, Layer? layer, CardFace cardface, string dir) {
        foreach (var span in cardface.Layout.Spans) {
            if (span.Condition == null || Resolver.EvaluateBoolean(span.Condition, cardface)) {
                DrawSpan(img, span, cardface);
            }
        }
        foreach (var box in cardface.Layout.TextBoxes) {
            if (box.Condition == null || Resolver.EvaluateBoolean(box.Condition, cardface)) {
                DrawBox(img, box, cardface);
            }
        }
    }

    private void DrawSpan(IImageProcessingContext img, Span span, CardFace cardface) {
        Log.Info($"    drawing span {span.Id}");

        var box = span.Position.Box;
        var rot = span.Position.Rotation is 90 or 270;
        var width = rot ? box.Height : box.Width;
        var height = rot ? box.Width : box.Height;
        var range = (0, width);

        using var image = new Image<Rgba32>(width, height);
        image.Mutate(im => {
            foreach (var elem in span.Elements.Where(el => el.Condition == null || Resolver.EvaluateBoolean(el.Condition, cardface))) {
                if (elem.Text != null) {
                    Log.Info("      drawing text element");
                    range = DrawTextElement(im, elem.Text, cardface, range, height, elem.Align);
                }
                else if (elem.Img != null) {
                    Log.Info("      drawing img element");
                    range = DrawImgElement(im, elem.Img, cardface, range, height, elem.Align);
                }
            }
            if (span.Position.Rotation != 0) {
                im.Rotate(span.Position.Rotation);
            }
        });

        img.DrawImage(image, box.Location, 1.0f);

        if (ConfigurationService.Instance.Config.ConfigParameters.Debug) {
            DrawOutline(img, Color.Blue, new Rectangle(box.X, box.Y, box.Width - 1, box.Height - 1));
            if (rot) {
                img.DrawLines(Pens.Solid(Color.Magenta, 1), new PointF(box.Left + box.Width / 2, box.Top), new PointF(box.Left + box.Width / 2, box.Bottom - 1));
            }
            else {
                img.DrawLines(Pens.Solid(Color.Magenta, 1), new PointF(box.Left, box.Top + box.Height / 2), new PointF(box.Right - 1, box.Top + box.Height / 2));
            }
        }
    }

    protected static (int, int) DrawTextElement(IImageProcessingContext img, TextElement element, CardFace cardface, (int min, int max) range, int height, Align align) {
        var color = DetermineColor(element.Font, cardface);
        var str = Resolver.EvaluateString(element.Value, cardface);
        if (str.Length > 0) {
            var paragraph = TextProcessor.ProcessParagraph(str, false, color);
            for (var fs = element.Font.Size; fs > element.Font.Size / 4; fs -= 2) {
                var parameters = new RenderParameters {
                    Width = int.MaxValue,
                    ParagraphMargins = new Margins {
                        Left = element.Margins?.Left ?? 0,
                        Right = element.Margins?.Right ?? 0,
                        Top = (int) (fs * 0.1f),
                        Bottom = (int) (fs * 0.1f)
                    },
                    Size = fs,
                    Fonts = new Dictionary<GlyphType, (Font font, string name, int ascender, int descender, int linegap, int height, int lineheight)> {
                        [GlyphType.Regular] = FontService.Instance.GetFontDetail(element.Font.RegularFont ?? ConfigurationService.Instance.Config.TextProperties.RegularFont, fs),
                        [GlyphType.Italic] = FontService.Instance.GetFontDetail(element.Font.ItalicFont ?? ConfigurationService.Instance.Config.TextProperties.ItalicFont, fs),
                        [GlyphType.Bold] = FontService.Instance.GetFontDetail(element.Font.BoldFont ?? ConfigurationService.Instance.Config.TextProperties.BoldFont, fs)
                    }
                };
                paragraph.Parameters = parameters;
                PreprocessParagraph(paragraph, true);
                if (paragraph.Bounds.Width <= range.max - range.min) {
                    RenderParagraph(paragraph);
                    var px = align.H switch {
                        AlignHorizontal.Left => range.min,
                        AlignHorizontal.Right => range.max - paragraph.Image!.Width,
                        _ => range.min + (range.max - range.min - paragraph.Image!.Width) / 2
                    };
                    var py = (int)(element.Adjust * fs) + align.V switch { // TODO implement align Top / Bottom
                        AlignVertical.Middle => height / 2 - parameters.Fonts[GlyphType.Regular].ascender / 2 - parameters.ParagraphMargins.Top,
                        _ => (height - paragraph.Image!.Height) / 2
                    };

                    if (element.Shadow != null) {
                        using var shadow = paragraph.Image!.Clone(im => im.DrawImage(new Image<Rgba32>(paragraph.Image!.Width, paragraph.Image!.Height, Color.Black), new GraphicsOptions { AlphaCompositionMode = PixelAlphaCompositionMode.SrcIn }));
                        img.DrawImage(shadow, new Point(px + element.Shadow.Value.X, py + element.Shadow.Value.Y), 1.0f);
                    }

                    img.DrawImage(paragraph.Image!, new Point(px, py), 1.0f);
                    return align.H == AlignHorizontal.Left ? (range.min + paragraph.Image!.Width, range.max) : (range.min, range.max - paragraph.Image!.Width);
                }
            }
        }
        return range;
    }

    protected static (int, int) DrawImgElement(IImageProcessingContext img, ImgElement element, CardFace cardface, (int min, int max) range, int height, Align align) {
        var path = Path.Combine(cardface.Layout.Dir, Resolver.EvaluateString(element.Path, cardface));
        var pads = element.Margins != null ? element.Margins.Left + element.Margins.Right : 0;
        using var image = Path.GetExtension(path).Equals(".svg") ? SvgUtil.DrawSvg(path, element.Height) : ImageUtil.LoadImageSimple(path, 0, element.Height);
        var px = align.H switch {
            AlignHorizontal.Left => range.min + (element.Margins?.Left ?? 0),
            AlignHorizontal.Right => range.max - image.Width - (element.Margins?.Right ?? 0),
            _ => range.min + (range.max - range.min - image.Width) / 2
        };
        var py = (height - image.Height) / 2 + element.Adjust; // TODO implement align Top / Bottom

        img.DrawImage(image, new Point(px, py), 1.0f);

        if (ConfigurationService.Instance.Config.ConfigParameters.Debug) {
            DrawOutline(img, Color.DarkGreen, new Rectangle(px, py, image.Width, image.Height));
        }

        return align.H == AlignHorizontal.Left ? (range.min + image.Width + pads, range.max) : (range.min, range.max - image.Width - pads);
    }

    protected abstract void DrawBox(IImageProcessingContext img, TextBox box, CardFace cardface);

    protected void PreprocessSections(List<Section> sections, Constrains constrains, TextBox box, int ifs) {
        for (var fs = ifs; fs > ifs / 4; fs -= 2) {
            // pre-process sections
            sections.ForEach(section => {
                section.Parameters = PrepareParameters(section, box, fs);
                PreprocessSection(section);
            });

            // check if sections fit inside constrains
            if (sections.Sum(s => s.Bounds.Height) <= constrains.Height) {
                Log.Info($"      using fontsize {fs}");
                sections.ForEach(RenderSection);
                return;
            }
        }
        Log.Info("      failed to select fontsize!");
    }

    private static RenderParameters PrepareParameters(Section section, TextBox box, int fs) {
        var sfs = box.FixedSections?.FirstOrDefault(f => f.FontSize != null && (f.Idx == section.Idx || string.Equals(f.Key, section.Key)))?.FontSize ?? fs;
        return new RenderParameters {
            Width = box.Position.Rotation is 90 or 270 ? box.Position.Box.Height : box.Position.Box.Width,
            Height = Math.Max(section.IndicatorImage?.Height ?? 0, box.FixedSections?.FirstOrDefault(f => f.Height != null && (f.Idx == section.Idx || string.Equals(f.Key, section.Key)))?.Height ?? 0),
            LineGapRatio = box.SectionParameters.LineGapRatio,
            ParagraphGapRatio = box.SectionParameters.ParagraphGapRatio,
            ParagraphMargins = new Margins {
                Left = box.SectionParameters.ParagraphMargins.Left + (section.Indent ? box.SectionParameters.Indentation : 0),
                Right = box.SectionParameters.ParagraphMargins.Right,
                Top = (int) (sfs * 0.1f), // TODO make configurable
                Bottom = (int) (sfs * 0.0f) // TODO make configurable
            },
            Size = sfs,
            Fonts = new Dictionary<GlyphType, (Font font, string name, int ascender, int descender, int linegap, int height, int lineheight)> {
                [GlyphType.Regular] = FontService.Instance.GetFontDetail(box.FontParameters.RegularFont ?? ConfigurationService.Instance.Config.TextProperties.RegularFont, sfs),
                [GlyphType.Italic] = FontService.Instance.GetFontDetail(box.FontParameters.ItalicFont ?? ConfigurationService.Instance.Config.TextProperties.ItalicFont, sfs),
                [GlyphType.Bold] = FontService.Instance.GetFontDetail(box.FontParameters.BoldFont ?? ConfigurationService.Instance.Config.TextProperties.BoldFont, sfs)
            }
        };
    }

    private static void PreprocessSection(Section section) {
        foreach (var paragraph in section.Paragraphs) {
            paragraph.Parameters = section.Parameters;
            PreprocessParagraph(paragraph);
        }
        section.Bounds = new Rectangle(0, 0, section.Parameters!.Width, Math.Max(section.Parameters.Height, CalculateParagraphsHeight(section, CalculateParagraphPadding(section.Parameters!))));
    }

    private static void PreprocessParagraph(Paragraph paragraph, bool shrink = false) {
        var parameters = paragraph.Parameters!;
        var margins = parameters.ParagraphMargins;

        var sw = (int)TextMeasurer.Measure(" ", new TextOptions(parameters.Fonts[GlyphType.Regular].font)).Width;
        var dw = (int)TextMeasurer.Measure("•", new TextOptions(parameters.Fonts[GlyphType.Regular].font)).Width;

        var pos = new Point(margins.Left, margins.Top);
        var max = 0;
        var first = true;
        foreach (var word in paragraph.Words) {
            foreach (var run in word.GlyphRuns) {
                if (run.Type == GlyphType.Symbol) {
                    var adj = (int) (parameters.Size * ConfigurationService.Instance.Config.TextProperties.SymbolMarginRatio);
                    run.AdjustLeft = adj / 2;
                    run.Width = SymbolService.Instance.CalculateWidth(run.Text, (int) (parameters.Size * SelectSymbolAdjust(run).Size)) + adj;
                }
                else {
                    run.Width = (int)TextMeasurer.Measure(run.Text, new TextOptions(parameters.Fonts[run.Type].font)).Width;
                }
            }

            // new line?
            if (parameters.Width - margins.Left - margins.Right < pos.X + word.GlyphRuns.Sum(r => r.Width) + (first ? 0 : sw)) {
                pos = new Point(margins.Left, (int) (pos.Y + parameters.Fonts[GlyphType.Regular].height + parameters.Fonts[GlyphType.Regular].linegap * parameters.LineGapRatio));
                if (paragraph.Words.First().GlyphRuns.First().Text.Equals("•")) {
                    pos.Offset(dw + sw, 0);
                }
            }
            else if (!first) {
                // offset 'space'
                pos.Offset(sw, 0);
            }

            // 'position' word
            word.Bounds = new Rectangle(pos.X, pos.Y, word.GlyphRuns.Sum(r => r.Width), parameters.Fonts[GlyphType.Regular].height);
            pos.Offset(word.Bounds.Width, 0);

            max = Math.Max(max, pos.X);

            first = false;
        }

        paragraph.Bounds = new Rectangle(0, 0, shrink ? max + margins.Right : parameters.Width, pos.Y + parameters.Fonts[GlyphType.Regular].height + margins.Bottom);
    }

    private static SymbolAdjust SelectSymbolAdjust(GlyphRun run) {
        return ConfigurationService.Instance.Config.TextProperties.SymbolAdjusts.Values.FirstOrDefault(sa => sa.Values?.Contains(run.Text) ?? false) ?? ConfigurationService.Instance.Config.TextProperties.SymbolAdjusts.Values.First(sa => sa.Default);
    }

    private static void RenderSection(Section section) {
        var padding = CalculateParagraphPadding(section.Parameters!);
        var pos = new Point(0, (section.Bounds.Height - CalculateParagraphsHeight(section, padding)) / 2);

        var image = new Image<Rgba32>(section.Bounds.Width, section.Bounds.Height);
        image.Mutate(img => {
            foreach (var paragraph in section.Paragraphs) {
                RenderParagraph(paragraph);
                img.DrawImage(paragraph.Image, pos, 1.0f);
                pos.Offset(0, paragraph.Image!.Height + padding);
            }

            if (ConfigurationService.Instance.Config.ConfigParameters.Debug) {
                DrawOutline(img, Color.Magenta, new Rectangle(0, 0, image.Width - 1, image.Height - 1));
            }
        });
        section.ContentsImage = image;
    }

    private static void RenderParagraph(Paragraph paragraph) {
        var parameters = paragraph.Parameters!;
        var image = new Image<Rgba32>(paragraph.Bounds.Width, paragraph.Bounds.Height);
        image.Mutate(img => {
            if (paragraph.Background) {
                // TODO make darkening level configurable
                img.Fill(Color.FromRgba(0, 0, 0, 64), new Rectangle(parameters.ParagraphMargins.Left / 2, 0, image.Width - (parameters.ParagraphMargins.Left + parameters.ParagraphMargins.Right) / 2, image.Height));
            }

            if (ConfigurationService.Instance.Config.ConfigParameters.Debug) {
                // draw margins
                DrawOutline(img, Color.Red, new Rectangle(parameters.ParagraphMargins.Left, parameters.ParagraphMargins.Top, image.Width - parameters.ParagraphMargins.Left - parameters.ParagraphMargins.Right, image.Height - parameters.ParagraphMargins.Top - parameters.ParagraphMargins.Bottom));
                // draw border
                DrawOutline(img, Color.LightBlue, new Rectangle(0, 0, image.Width - 1, image.Height - 1));
            }

            var prev = new Point(0, int.MinValue);
            foreach (var word in paragraph.Words) {
                var pos = word.Bounds.Location;
                if (ConfigurationService.Instance.Config.ConfigParameters.Debug && pos.Y != prev.Y) {
                    // draw guidelines
                    img.DrawLines(Color.Green,  1, new Point(parameters.ParagraphMargins.Left, pos.Y),                                                new Point(image.Width - parameters.ParagraphMargins.Right, pos.Y));
                    img.DrawLines(Color.Orange, 1, new Point(parameters.ParagraphMargins.Left, pos.Y + parameters.Fonts[GlyphType.Regular].ascender), new Point(image.Width - parameters.ParagraphMargins.Right, pos.Y + parameters.Fonts[GlyphType.Regular].ascender));
                    img.DrawLines(Color.Green,  1, new Point(parameters.ParagraphMargins.Left, pos.Y + parameters.Fonts[GlyphType.Regular].height),   new Point(image.Width - parameters.ParagraphMargins.Right, pos.Y + parameters.Fonts[GlyphType.Regular].height));
                    prev = pos;
                }

                if (ConfigurationService.Instance.Config.ConfigParameters.Debug) {
                    // draw word bounds
                    DrawOutline(img, Color.Cyan, new Rectangle(pos.X, pos.Y, word.GlyphRuns.Sum(r => r.Width), parameters.Fonts[GlyphType.Regular].height));
                }

                // draw word
                foreach (var run in word.GlyphRuns) {
                    if (run.Type == GlyphType.Symbol) {
                        var adjust = SelectSymbolAdjust(run);
                        var p = new Point(pos.X + run.AdjustLeft, (int) (pos.Y + parameters.Size * adjust.Top));
                        img.DrawImage(SymbolService.Instance.GetSymbol(run.Text, (int) (parameters.Size * adjust.Size)), p, 1.0f);
                    }
                    else {
                        // TODO 'hack' for the | rendering vertical position
                        var p = new Point(pos.X + run.AdjustLeft, run.Text.Equals("|") ? (int)(pos.Y - parameters.Fonts[run.Type].font.Size * 0.05) : pos.Y);
                        // TODO 'hack' for non-breaking space rendering vertical position
                        img.DrawText(run.Text.Replace("\u00A0", " "), parameters.Fonts[run.Type].font, run.Color, p);
                    }
                    pos.Offset(run.Width, 0);
                }
            }
        });
        paragraph.Image = image;
    }

    private static int CalculateParagraphPadding(RenderParameters parameters) {
        return (int) (parameters.Size * parameters.ParagraphGapRatio);
    }

    private static int CalculateParagraphsHeight(Section section, int padding) {
        return section.Paragraphs.Sum(p => p.Bounds.Height) + (section.Paragraphs.Count - 1) * padding;
    }

    protected static void DrawOutline(IImageProcessingContext img, Color color, Rectangle rect) {
        img.DrawLines(color, 1, new PointF(rect.X, rect.Y), new PointF(rect.X, rect.Y + rect.Height));
        img.DrawLines(color, 1, new PointF(rect.X, rect.Y + rect.Height), new PointF(rect.X + rect.Width, rect.Y + rect.Height));
        img.DrawLines(color, 1, new PointF(rect.X + rect.Width, rect.Y + rect.Height), new PointF(rect.X + rect.Width, rect.Y));
        img.DrawLines(color, 1, new PointF(rect.X + rect.Width, rect.Y), new PointF(rect.X, rect.Y));
    }

    private static Color DetermineColor(FontParameters font, CardFace cardface) {
        return Color.Parse(Resolver.EvaluateString(font.Color ?? "black", cardface));
    }
}