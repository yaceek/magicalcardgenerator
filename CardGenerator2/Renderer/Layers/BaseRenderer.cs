﻿using CardGenerator2.Model.Config;
using CardGenerator2.Model.Renderer;
using CardGenerator2.Utils;
using NLog;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;

namespace CardGenerator2.Renderer.Layers;

internal interface IRenderer {
    public void Render(IImageProcessingContext img, FrameLayer layer, CardFace cardface);
}

internal abstract class BaseRenderer : IRenderer {
    protected Logger Log { get; }

    protected BaseRenderer() {
        Log = LogManager.GetLogger(GetType().ToString());
    }

    public void Render(IImageProcessingContext img, FrameLayer layer, CardFace cardface) {
        var dir = cardface.Layout.Dir;
        if (layer.Condition == null || Resolver.EvaluateBoolean(layer.Condition, cardface)) {
            if (layer.Selector != null) {
                foreach (var selector in layer.Selector) {
                    if (selector.Condition == null || Resolver.EvaluateBoolean(selector.Condition, cardface)) {
                        Log.Info($"    drawing layer {layer.Id}");
                        RenderInternal(img, selector, cardface, dir);
                        break;
                    }
                }
            }
            else {
                Log.Info($"    drawing layer {layer.Id}");
                RenderInternal(img, null, cardface, dir);
            }
        }
        else {
            Log.Info($"    ignoring layer {layer.Id}");
        }
    }

    protected abstract void RenderInternal(IImageProcessingContext img, Layer? layer, CardFace cardface, string dir);

    protected static void DrawImage(IImageProcessingContext img, Rectangle bounds, bool fit, int rotation, Image image, Image? mask = null, PixelColorBlendingMode blend = PixelColorBlendingMode.Normal, float opacity = 1.0f) {
        var point = bounds.Location;
        if (rotation != 0) {
            image.Mutate(i => i.Rotate(rotation));
        }
        if (image.Width != bounds.Width || image.Height != bounds.Height) {
            if (fit) {
                image.Mutate(i => i.Resize(new ResizeOptions { Mode = ResizeMode.Max, Size = bounds.Size }));
                point.Offset((bounds.Width - image.Width) / 2, (bounds.Height - image.Height) / 2);
            }
            else {
                image.Mutate(i => i.Resize(new ResizeOptions { Mode = ResizeMode.Stretch, Size = bounds.Size }));
            }
        }

        if (mask != null) {
            mask.Mutate(im => im.DrawImage(image, point, new GraphicsOptions { AlphaCompositionMode = PixelAlphaCompositionMode.SrcIn }));
            img.DrawImage(mask, opacity);
        }
        else {
            img.DrawImage(image, point, blend, opacity);
        }
    }
}
