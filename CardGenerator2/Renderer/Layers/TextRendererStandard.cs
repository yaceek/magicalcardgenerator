﻿using CardGenerator2.Model.Config;
using CardGenerator2.Model.Renderer;
using CardGenerator2.Model.Text;
using CardGenerator2.Services;
using CardGenerator2.Utils;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Drawing.Processing;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;

namespace CardGenerator2.Renderer.Layers;

internal class TextRendererStandard : TextRenderer2 {
    protected override void DrawBox(IImageProcessingContext img, TextBox box, CardFace cardface) {
        Log.Info($"    drawing standard textbox {box.Id}");

        // resolve text sections
        var sections = Resolver.EvaluateObject<List<Section>>(box.Source, cardface);

        // prepare constrains
        var constrains = new Constrains {
            Width = box.Position.Rotation is 90 or 270 ? box.Position.Box.Height : box.Position.Box.Width,
            Height = (box.Position.Rotation is 90 or 270 ? box.Position.Box.Width : box.Position.Box.Height) - (box.Separators?.Take(..(sections.Count - 1)).Sum(s => s.Height) ?? 0),
            HeightTotal = box.Position.Rotation is 90 or 270 ? box.Position.Box.Width : box.Position.Box.Height
        };

        // draw sections
        DrawSections(img, sections, constrains, box, cardface);
    }

    private void DrawSections(IImageProcessingContext img, List<Section> sections, Constrains constrains, TextBox box, CardFace cardface) {
        if (sections.Count > 0) {
            PreprocessSections(sections, constrains, box, cardface.FieldInt("textbox_fontsize") ?? box.FontParameters.Size);

            var pos = new Point(0, (constrains.Height - sections.Sum(s => s.ContentsImage!.Height)) / 2);

            using var boximage = new Image<Rgba32>(constrains.Width, constrains.HeightTotal);
            boximage.Mutate(im => {
                for (var i = 0; i < sections.Count; i++) {
                    var section = sections[i];
                    if (i > 0 && box.Separators != null) {
                        var separator = box.Separators[i - 1];
                        if (!cardface.FieldTrue("no_separator") && separator.Image != null) {
                            DrawSeparator(im, pos, constrains.Width, separator, cardface);
                        }
                        pos.Offset(0, separator.Height);
                    }
                    im.DrawImage(section.ContentsImage, pos, 1.0f);
                    pos.Offset(0, section.ContentsImage!.Height);
                }
                if (box.Position.Rotation != 0) {
                    im.Rotate(box.Position.Rotation);
                }
            });
            img.DrawImage(boximage, box.Position.Box.Location, 1.0f);
        }

        if (ConfigurationService.Instance.Config.ConfigParameters.Debug) {
            DrawOutline(img, Color.Blue, box.Position.Box);
        }
    }

    private static void DrawSeparator(IImageProcessingContext img, Point position, int width, Separator separator, CardFace cardface) {
        // load separator image
        using var si = ImageUtil.LoadImageSimple(Path.Combine(cardface.Layout.Dir, separator.Image!), width - (separator.Margins?.Left ?? 0) - (separator.Margins?.Right ?? 0));

        // draw the separator image
        var point = separator.Align switch {
            AlignVertical.Center => new Point(position.X + (separator.Margins?.Left ?? 0), position.Y + (separator.Height - si.Height) / 2),
            AlignVertical.Bottom => new Point(position.X + (separator.Margins?.Left ?? 0), position.Y + separator.Height - si.Height),
            _ => new Point(position.X + (separator.Margins?.Left ?? 0), position.Y)
        };
        img.DrawImage(si, point, 1.0f);

        if (ConfigurationService.Instance.Config.ConfigParameters.Debug) {
            DrawOutline(img, Color.Yellow, new Rectangle(point, new Size(si.Width, si.Height)));
            img.DrawLines(Pens.Solid(Color.Magenta, 1), new PointF(point.X, point.Y + si.Height / 2), new PointF(point.X + si.Width, point.Y + si.Height / 2));
        }
    }
}