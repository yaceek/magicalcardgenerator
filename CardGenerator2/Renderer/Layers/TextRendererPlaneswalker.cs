﻿using System.Diagnostics.CodeAnalysis;
using CardGenerator2.Model.Config;
using CardGenerator2.Model.Renderer;
using CardGenerator2.Model.Text;
using CardGenerator2.Renderer.Layers.Extras;
using CardGenerator2.Services;
using CardGenerator2.Utils;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Drawing.Processing;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;

namespace CardGenerator2.Renderer.Layers;

internal class TextRendererPlaneswalker : TextRenderer2 {
    protected override void DrawBox(IImageProcessingContext img, TextBox box, CardFace cardface) {
        Log.Info($"    drawing planeswalker textbox {box.Id}");

        // resolve text sections
        var sections = Resolver.EvaluateObject<List<Section>>(box.Source, cardface);
        var indicated = sections.FindAll(s => !s.Key.Equals("_")); // TODO mark sections with indicators in TextProcessor

        // prepare constrains
        var constrains = new Constrains {
            Width = box.Position.Box.Width,
            Height = box.Position.Box.Height - (box.Separators?.Take(..(sections.Count - 1)).Sum(s => s.Height) ?? 0),
            HeightTotal = box.Position.Box.Height
        };

        // prepare indicators
        if (box.Indicator?.Images != null) {
            IndicatorSupport.PrepareIndicators(indicated, box.Indicator, cardface);
        }

        // draw sections
        DrawSections(img, sections, constrains, box, cardface);

        // draw indicators
        if (box.Indicator?.Images != null) {
            IndicatorSupport.DrawIndicators(img, indicated, box.Indicator);
        }
    }

    [SuppressMessage("ReSharper", "AccessToDisposedClosure")]
    [SuppressMessage("ReSharper", "AccessToModifiedClosure")]
    private void DrawSections(IImageProcessingContext img, List<Section> sections, Constrains constrains, TextBox box, CardFace cardface) {
        if (sections.Count > 0) {
            PreprocessSections(sections, constrains, box, cardface.FieldInt("textbox_fontsize") ?? box.FontParameters.Size);

            // background mask
            using var mask = ImageUtil.LoadImageSimple(Path.Combine(cardface.Layout.Dir, Resolver.EvaluateString(box.BackgroundMask, cardface)));
            using var filler = ImageUtil.LoadImageSimple(Path.Combine(cardface.Layout.Dir, Resolver.EvaluateString(box.BackgroundFiller, cardface)));

            // layers to draw on text and background
            using var text = new Image<Rgba32>(cardface.Layout.Dimensions.Width, cardface.Layout.Dimensions.Height);
            using var background = new Image<Rgba32>(cardface.Layout.Dimensions.Width, cardface.Layout.Dimensions.Height);

            var top = Point.Empty;
            var pos = box.Position.Box.Location;
            var pad = (constrains.Height - sections.Sum(s => s.ContentsImage!.Height)) / (2 * sections.Count);

            for (var i = 0; i < sections.Count; i++) {
                var first = i == 0;
                var last = i == sections.Count - 1;
                var section = sections[i];

                // separator
                if (!first) {
                    if (box.Separators != null) {
                        var separator = box.Separators[i - 1];
                        if (separator.Image != null) {
                            // draw separator image as background
                            background.Mutate(bi => {
                                using var si = ImageUtil.LoadImageSimple(Path.Combine(cardface.Layout.Dir, separator.Image));
                                bi.DrawImage(si, pos, 1.0f);
                            });
                        }
                        if (ConfigurationService.Instance.Config.ConfigParameters.Debug) {
                            DrawOutline(img, Color.Red, new Rectangle(pos, new Size(constrains.Width, separator.Height)));
                        }

                        pos.Offset(0, separator.Height);
                    }
                    top = pos;
                }

                // pad section top
                pos.Offset(0, pad);

                // draw section image
                text.Mutate(im => im.DrawImage(section.ContentsImage, pos, 1.0f));
                section.Middle = pos.Y + section.ContentsImage!.Height / 2;
                pos.Offset(0, section.ContentsImage.Height);

                // pad section bottom
                pos.Offset(0, pad);

                // draw filler
                if (i % 2 == 0) {
                    filler.Mutate(im => im.Resize(background.Width,  (last ? background.Height : pos.Y) - top.Y));
                    background.Mutate(im => im.DrawImage(filler, top, 1.0f));
                }
            }

            // trim background using textbox mask
            mask.Mutate(im => im.DrawImage(background, new GraphicsOptions { AlphaCompositionMode = PixelAlphaCompositionMode.SrcIn }));

            // draw background
            img.DrawImage(mask, 1.0f);

            // draw text
            img.DrawImage(text, 1.0f);
        }

        if (ConfigurationService.Instance.Config.ConfigParameters.Debug) {
            img.Draw(Pens.Solid(Color.Blue, 1), box.Position.Box);
        }
    }
}
