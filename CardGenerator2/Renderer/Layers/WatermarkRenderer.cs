﻿using System.Diagnostics.CodeAnalysis;
using CardGenerator2.Model.Config;
using CardGenerator2.Model.Renderer;
using CardGenerator2.Utils;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;

namespace CardGenerator2.Renderer.Layers;

internal class WatermarkRenderer : BaseRenderer {
    [SuppressMessage("ReSharper", "AccessToDisposedClosure")]
    protected override void RenderInternal(IImageProcessingContext img, Layer? layer, CardFace cardface, string dir) {
        var bounds = layer!.Position?.Box ?? cardface.Layout.Dimensions;

        using var image = SvgUtil.DrawSvg(Path.Combine(dir, Resolver.EvaluateString(layer.Source, cardface)), bounds.Height);

        DrawImage(img, bounds, layer.Fit, 0, image, null, PixelColorBlendingMode.Overlay, 0.7f);
        DrawImage(img, bounds, layer.Fit, 0, image, null, PixelColorBlendingMode.Overlay, 0.7f);
        DrawImage(img, bounds, layer.Fit, 0, image, null, PixelColorBlendingMode.Normal, 0.05f);
    }
}
