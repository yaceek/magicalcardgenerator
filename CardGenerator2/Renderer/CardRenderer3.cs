﻿using CardGenerator2.Model.Config;
using CardGenerator2.Model.Renderer;
using CardGenerator2.Renderer.Layers;
using CardGenerator2.Services;
using CardGenerator2.Utils;
using NLog;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;

namespace CardGenerator2.Renderer;

internal class CardRenderer3 {
    private static readonly Logger Log = LogManager.GetCurrentClassLogger();
    private readonly Output.Output _output;

    public CardRenderer3(Output.Output output) {
        _output = output;
    }

    public void RenderCardFace(CardFace cardface, int? count) {
        Log.Info($"  Processing {cardface.FullCardName} [{cardface.Face}]...");

        foreach (var key in cardface.Layout.Vars.Keys) {
            cardface.Vars[key] = Resolver.EvaluateString(cardface.Layout.Vars[key], cardface);
        }

        var cardimage = new Image<Rgba32>(cardface.Layout.Dimensions.Width, cardface.Layout.Dimensions.Height);
        cardimage.Mutate(img => {
            foreach (var layer in cardface.Layout.FrameLayers) {
                SelectRenderer(layer)?.Render(img, layer, cardface);
            }
        });

        Log.Info("    resizing/cropping/padding frame");
        ResizeImage(cardimage, cardface.Layout, Color.Parse(cardface.Field("border_color")));

        Log.Info("    saving image");
        _output.StoreImage(cardimage, $"{cardface.Field("set")!.ToUpper()}-{cardface.Field("collector_number", 3)} {cardface.FullCardName.Replace(" ", "_").Replace("//", "~").Replace(":", "")} [{cardface.Face}]", count);

        Log.Info($"  Done {cardface.FullCardName} [{cardface.Face}]...");
    }

    private static IRenderer? SelectRenderer(FrameLayer layer) {
        return layer.Type switch {
            "art" => new ArtRenderer(),
            "overlay" => new OverlayRenderer(),
            "frame" => new FrameRenderer(),
            "watermark" => new WatermarkRenderer(),
            "text-pw" => new TextRendererPlaneswalker(),
            "text-vert" => new TextRendererVertical(),
            "text" => new TextRendererStandard(),
            _ => null
        };
    }

    private static void ResizeImage(Image frame, Layout layout, Color color) {
        // if configured target dpi is zero, then no adjustments
        var config = ConfigurationService.Instance.Config.ConfigParameters;
        if (config.TargetDPI != 0) {
            var width = frame.Width * config.TargetDPI / layout.Meta.DPI;
            var height = frame.Height * config.TargetDPI / layout.Meta.DPI;

            // 1. scale
            if (config.TargetDPI != layout.Meta.DPI) {
                frame.Mutate(im => im.Resize(width, height));
            }

            // 2. apply margin
            if (config.TargetMargin != layout.Meta.Margin) {
                var margin = config.TargetMargin - layout.Meta.Margin * config.TargetDPI / layout.Meta.DPI;
                if (margin > 0) {
                    frame.Mutate(im => im.Pad(width + 2 * margin, height + 2 * margin, color));
                }
                else if (margin < 0) {
                    frame.Mutate(im => im.Crop(new Rectangle(-margin, -margin, frame.Width + 2 * margin, frame.Height + 2 * margin)));
                }
            }
        }
    }
}
